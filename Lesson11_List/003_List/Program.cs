﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _003_List
{
    class Program
    {
        static void Main(string[] args)
        {
            var list1 = new ArrayList<int> { 10, 20, 30};
            list1.Add(40);
            list1.Add(50);
            list1.Add(60);
        }
    }
}
