﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _006_List
{
    class Program
    {
        static void Main(string[] args)
        {
            var list1 = new ArrayList<int> { 10, 20, 30 };
            foreach (int item in list1)
            {
                Console.WriteLine(item);
            }

            Console.ReadLine();
        }
    }
}
