﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _002_List
{
    class Program
    {
        static void Main(string[] args)
        {
            var list1 = new ArrayList<int>(10);

            var coll = new List<int> { 10, 20, 30 };
            var list2 = new ArrayList<int>(coll);
        }
    }
}
