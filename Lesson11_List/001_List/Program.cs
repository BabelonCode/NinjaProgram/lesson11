﻿using System;
using System.Collections.Generic;

namespace _001_List
{
    class Program
    {
        static void Main(string[] args)
        {
            var list = new List<int>();
            for (int i = 0; i < int.MaxValue; i++)
            {
                list.Add(i);
            }

            Console.ReadLine();
        }
    }
}
