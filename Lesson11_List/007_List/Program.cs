﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace _007_List
{
    //https://referencesource.microsoft.com/#mscorlib/system/collections/generic/list.cs,cf7f4095e4de7646
    class Program
    {
        static void Main(string[] args)
        {
            int count = 180;
            var rnd = new Random();
            var worker = new Worker();

            var tasks = new Task[count];
            for (int i = 0; i < count; i += 3)
            {
                tasks[i] = Task.Factory.StartNew(() => WorkerPrintAction(worker, rnd));
                tasks[i + 1] = Task.Factory.StartNew(() => WorkerAddAction(worker, rnd));
                tasks[i + 2] = Task.Factory.StartNew(() => WorkerRemoveAction(worker, rnd));
            }

            Console.ReadLine();

        }

        static void WorkerAddAction(Worker worker, Random rnd)
        {
            int item = rnd.Next(0, 3);
            worker.TryAdd(item);
        }

        static void WorkerRemoveAction(Worker worker, Random rnd)
        {
            int index = rnd.Next(0, 3);
            worker.TryRemove(index);
        }

        static void WorkerPrintAction(Worker worker, Random rnd)
        {
            int item = rnd.Next(0, 3);
            worker.TryPrint(item);
        }


        class Worker
        {
            private List<int> list = new List<int>();

            public void TryAdd(int item)
            {
                list.Add(item);
            }

            public void TryPrint(int index)
            {
                //if (list.Contains(item))
                //    Console.WriteLine($"[Worker] value = {item}");
                if (index < list.Count)
                    Console.WriteLine($"[Worker] value = {list[index]}");
            }

            public void TryRemove(int item)
            {
                list.Remove(item);
            }
        }

    }
}
